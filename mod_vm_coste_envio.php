<?php

defined('_JEXEC') or  die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
if (!class_exists( 'VmConfig' )) require(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart'.DS.'helpers'.DS.'config.php');
if (!class_exists('VirtueMartCart')) require(JPATH_VM_SITE.DS.'helpers'.DS.'cart.php');
require_once(JPATH_SITE.'/plugins/vmshipment/weight_countries/weight_countries.php');

JPluginHelper::importPlugin('vmpayment');
$config = JPluginHelper::getPlugin('vmshipment');

$zip = JRequest::getInt('vmcecp', 0, 'cookie');

class JoniJnmVMPrecioPorPostal extends plgVmShipmentWeight_countries {
	protected function checkConditions ($cart, $method, $cart_prices) {
		$this->convert ($method);
		$orderWeight = $this->getOrderWeight ($cart, $method->weight_unit);
		$type = (($cart->ST == 0) ? 'BT' : 'ST');

		$weight_cond = $this->testRange($orderWeight,$method,'weight_start','weight_stop','weight');
		$nbproducts_cond = $this->_nbproductsCond ($cart, $method);
		if(isset($cart_prices['salesPrice'])){
			$orderamount_cond = $this->testRange($cart_prices['salesPrice'],$method,'orderamount_start','orderamount_stop','order amount');
		} else {
			$orderamount_cond = FALSE;
		}
		$zip = JRequest::getInt('vmcecp', 0, 'cookie');
		$userFieldsModel =VmModel::getModel('Userfields');
		if ($userFieldsModel->fieldPublished('zip', $type)){
			if (isset($zip)) {
				$zip_cond = $this->testRange($zip,$method,'zip_start','zip_stop','zip');
			} else {

				$zip_cond = false;
			}
		} else {
			$zip_cond = true;
		}

		$allconditions = (int) $weight_cond + (int)$zip_cond + (int)$nbproducts_cond + (int)$orderamount_cond + 1;
		if($allconditions === 5){
			return TRUE;
		} else {

			return FALSE;
		}

		vmdebug('checkConditions '.$method->name.' does not fit');
		return FALSE;
	}
	
	private function testRange($value, $method, $floor, $ceiling,$name){

		$cond = true;
		if(!empty($method->$floor) and !empty($method->$ceiling)){
			$cond = (($value >= $method->$floor AND $value <= $method->$ceiling));
			if(!$cond){
				$result = 'FALSE';
				$reason = 'is NOT within Range of the condition from '.$method->$floor.' to '.$method->$ceiling;
			} else {
				$result = 'TRUE';
				$reason = 'is within Range of the condition from '.$method->$floor.' to '.$method->$ceiling;
			}
		} else if(!empty($method->$floor)){
			$cond = ($value >= $method->$floor);
			if(!$cond){
				$result = 'FALSE';
				$reason = 'is not at least '.$method->$floor;
			} else {
				$result = 'TRUE';
				$reason = 'is over min limit '.$method->$floor;
			}
		} else if(!empty($method->$ceiling)){
			$cond = ($value <= $method->$ceiling);
			if(!$cond){
				$result = 'FALSE';
				$reason = 'is over '.$method->$ceiling;
			} else {
				$result = 'TRUE';
				$reason = 'is lower than the set '.$method->$ceiling;
			}
		} else {
			$result = 'TRUE';
			$reason = 'no boundary conditions set';
		}

		vmdebug('shipmentmethod '.$method->shipment_name.' = '.$result.' for variable '.$name.' = '.$value.' Reason: '.$reason);
		return $cond;
	}
	
	private function _nbproductsCond ($cart, $method) {

		if (!isset($method->nbproducts_start) and !isset($method->nbproducts_stop)) {
			vmdebug('_nbproductsCond',$method);
			return true;
		}

		$nbproducts = 0;
		foreach ($cart->products as $product) {
			$nbproducts += $product->quantity;
		}

		if ($nbproducts) {

			$nbproducts_cond = $this->testRange($nbproducts,$method,'nbproducts_start','nbproducts_stop','products quantity');

		} else {
			$nbproducts_cond = false;
		}

		return $nbproducts_cond;
	}
}

$dispatcher = JDispatcher::getInstance();
$plugin = new JoniJnmVMPrecioPorPostal($dispatcher, (array) ($config[0]));
$cart = VirtueMartCart::getCart();
$pays = array();
$plugin->plgVmDisplayListFEShipment($cart, 0, $pays);
require(JModuleHelper::getLayoutPath('mod_vm_coste_envio'));